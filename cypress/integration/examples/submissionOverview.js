const submission = require('../../support/Pages/submission.js')
const article = require('../../support/Pages/article.js')
const ArticleInfo = require('../../fixtures/articleInfo.json')
const events = require('../../support/Pages/events.js')
const SubmissionInfo = require('../../fixtures/submissionInfo.json')

describe('Verify Submission and Articles pages and functionalities', () => {
   
    beforeEach(() => {
        cy.visit('https://next.morressier.com/event/ictf2021/5f33b6be11826e46af50a9a7')
    })

    it('Verify Submission overview', () => {
        /*
        * basic verification to see if the element exist or are visible to interact with them 
        */
        cy.get('[data-test-id="COOKIE_BANNER"]').should('exist').click()
        cy.get(events.Submission_Tab()).click()
        cy.get(submission.SearchField()).should('be.visible')
        cy.get('[type="button"]').should('be.visible')
        cy.get(submission.PosterGridContainer()).should('exist')
        //verification for a value hardcoded for the first time
        cy.get(submission.SearchFilterContainer()).next().should('contain',SubmissionInfo.submission_default_path)
        cy.get(submission.Pagination()).should('exist')
        
          //validating data  for the first div inside of the poster grid container
          //this allow me to do a E2E partiture partition technique to evalue one element of the grid
        
        cy.get(submission.PosterSubmissionArticle())
            .should('be.visible')
        cy.get(submission.PosterCardSubmission())
            .find('p').should('contain', SubmissionInfo.Category)
        cy.get(submission.EventPagePosterTitle())
            .should('contain', SubmissionInfo.Submission_title)
            .next().and('contain', SubmissionInfo.Authors)

    })
    it('Verify Article content', () => {
        /*
        *compare if the description on the article match with the value on the json file, this could be replaced by route call
        */
        cy.get('[data-test-id="COOKIE_BANNER"]').should('exist').click()
        cy.get(events.Submission_Tab()).click()
        cy.wait(1000)
        cy.get(submission.ContainerArticle(),{responseTimeout:6000}).find('> div').first().click()
        cy.wait(1000)
        cy.get(article.SectionContainerArtice()).find('> p').contains(SubmissionInfo.Category.charAt(0).toUpperCase() + SubmissionInfo.Category.slice(1).toLowerCase())
        cy.get(article.SectionContainerArtice()).find('h1').contains(SubmissionInfo.Submission_title)
        cy.get(article.ArticleSummaryContainer()).should('exist')
        cy.get(article.PlayerComponent()).should('be.visible')
        cy.get(article.PresentationAt()).find('> p').should('contain', ArticleInfo.presentation)
            .next().and('contain', ArticleInfo.article_event)
        cy.get(article.PDFComponent()).should('exist').and('be.visible')
        cy.get(article.FullDescription()).invoke('text').then((text) => {
            expect(text.trim()).equal(ArticleInfo.article_description)
        })
        cy.get(article.KeywordsComponent()).should('be.visible')
        cy.get(article.LicenseComponent()).should('exist')
        cy.get(submission.EventPagePosterTitle()).should('contain', ArticleInfo.next_event_title)
        cy.go('back')
        cy.get(submission.ContainerArticle()).find('> div').first().should('be.visible')
    })

    it('Search Article', () => {
        /*
        * Search article and check if the items on the grid match with the label for searched content
        */
        cy.get('[data-test-id="COOKIE_BANNER"]').should('exist').click()
        cy.get(events.Submission_Tab()).click()
        cy.get(submission.SearchFilterContainer()).find('input').type(SubmissionInfo.Submission_title + '{enter}')
        submission.CompareItems()
    })

    it(' search not exist value', () => {
         /*
        * Search article that doesn't exist
        */
        cy.get('[data-test-id="COOKIE_BANNER"]').should('exist').click()
        cy.get(events.Submission_Tab()).click()
        cy.get(submission.SearchFilterContainer()).find('input')
            .clear().type(SubmissionInfo.search_wrong_value + '{enter}')
        cy.wait(1000)
        cy.get(submission.SearchFilterContainer()).next().invoke('text').then((FilterItems) => {
            expect(FilterItems).to.contains(SubmissionInfo.submission_count_sad_path)//here compare if contains 0 as a value
        });
        cy.get(submission.SearchFilterContainer()).find('input').clear().type('{enter}')
    })

    it('Verify filter component', () => {
        cy.get('[data-test-id="COOKIE_BANNER"]').should('exist').click()
        cy.get(events.Submission_Tab()).click()
        cy.get(submission.FilterButton()).click()
        cy.get('.MuiDrawer-paper').should('exist')
        cy.get(submission.FilterKeywords()).type(SubmissionInfo.keywords);//adding keywords 
        cy.wait(500)//performance is too fast and don't wait some times for the element contain the data for that reason I use the wait
        cy.get(submission.ParentComponent()).find(submission.ListButton()).contains(SubmissionInfo.keywords).click()//click on check to add 
        cy.get('.MuiExpansionPanelSummary-content').invoke('click'); 
        cy.get(submission.AuthorName()).type(SubmissionInfo.author1)
        cy.wait(1000)
        cy.get(submission.ParentComponent()).find(submission.ListButton()).contains(SubmissionInfo.author1).click({timeout:6000})//adding authors1         
        cy.get(submission.AuthorName()).clear()       
        cy.get(submission.AuthorName()).type(SubmissionInfo.author2);
        cy.wait(1000)
        cy.get(submission.ParentComponent()).find(submission.ListButton()).contains(SubmissionInfo.author2).click({timeout:6000})//adding authors2
        cy.get(submission.AuthorName()).clear()  
        cy.get(submission.AuthorName()).type(SubmissionInfo.author3);
        cy.wait(1000)
        cy.get(submission.ParentComponent()).find(submission.ListButton()).contains(SubmissionInfo.author3).click({timeout:6000})//adding authors3
        cy.get('button').contains('Close').click()
        submission.CompareItems()//compare if the total of items filtered match with the items on the grid
    })

    it('Verify Playback ', () => {
        /*
        * the important for playback content is
        *1.verify if the classes changed after the interaction from start to play and play to pause
        *2.could We include some validation to making a a reverse action of the visual testing, 
        *checking if the items are diferents thats mind the content is reproducing + html validations 
        */
        var number =Math.random();  
        cy.get('[data-test-id="COOKIE_BANNER"]').should('exist').click()
        cy.get(events.Submission_Tab()).click()
        cy.wait(500)
        cy.get(submission.ContainerArticle()).find('> div').first().click()
        cy.get(article.PlayerComponent()).should('be.visible')//player component exist and ready to interact
        cy.get(article.Player()).should('be.visible')//player div ready to interact
        cy.get(article.LoadingSpinner()).should('contain','Video Player is loading.')
        cy.get(article.PlayDiv()).should('be.visible').click()//click on player
        cy.get(article.Player()).should('have.class', 'vjs-playing')//class change to playing
        cy.wait(2000)//to make the test more real
        cy.get(article.Player()).matchImageSnapshot('Player Component'+ number,{dumpDiffToConsole:true})//here I take a first action the compare snapchots
        cy.wait(5000)//making real the watching video
        cy.get(article.Player()).click()//pausing video
        cy.get(article.Player()).should('have.class','vjs-paused')//verify the class now change to pause
        cy.get(article.Player()).matchImageSnapshot('Player Component'+ number,{dumpDiffToConsole:true}).then((res)=>{
            /*
            * the normal way to check visual testing will throw error if the images are differents, for that reason  
            *{dumpDiffToConsole:true} was set up and added this in the package.json --env failOnSnapshotDiff=false"
            *then lete me manipulate the library wiith my assertions
            */

            expect(res.diffPixelCount).to.not.equal(0);//the player snapchot throw a different values to compare one of this is "diffPixelCount"
            //diffPixelCount >0  means actions are afecting the player, ==0 means nothing is happening 
            

        })

    })


})