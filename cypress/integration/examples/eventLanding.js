const events = require('../../support/Pages/events.js')
const session = require('../../support/Pages/session.js')
const morresierInfo = require('../../support/Pages/morresier.js')
const utils = require('../../fixtures/eventInfo.json')
const days = require('../../fixtures/days.js')

describe('Verify Event landing page and functionalities', () => {

    beforeEach(() => {
        cy.visit('https://next.morressier.com/event/ictf2021/5f33b6be11826e46af50a9a7')
    })

    it('Verify Event Landing Page sections ', () => {

       
        /*
        *Assertions for the Signup Modal 
        */
        cy.get('button').contains('Signup').click()
        cy.get(events.signUpModal()).should('exist').then(() => {
            cy.get(events.signUpModal()).contains('Log in').should('be.visible')
            cy.get(events.signUpModal()).find('[name="email"]').should('be.visible')
            cy.get(events.signUpModal()).find('[type="submit"]').should('be.visible')
            cy.get(events.signUpModal()).find(events.CloseModalButton()).click()
        });

        /*
        *Assertions for the Conference info, matching with hardcoded values using contains
         */
        cy.get(events.conferenceLogo()).should('be.visible')
        cy.get(events.conferenceTitle()).should('exist').and('contain', utils.event_title)
        cy.get(events.SecondHeader()).contains('August 16-20, 2021')
        cy.get(events.SecondHeader()).next().invoke('text').then((text) => {
            expect(text.trim()).equal(utils.event_description)
        });

        /*
         *Assertion dor the Join Discussion button with contains 
         */
        cy.get('.style__HeaderText-sc-1fffutc-3')
            .find('button').should('exist').click().then(() => {
                cy.get(events.JoinDiscussion()).contains(utils.event_title)
                cy.get(events.JoinDiscussion()).should('be.visible').find('button').click()
            });

        /*
        * Assertions to verify if all the tabs exist and are visible to interact with them using chai assertions
         */
        cy.get(events.Session_Tab()).should('exist').and('have.class', 'Mui-selected')
        cy.get(events.Submission_Tab()).should('exist').and('be.visible')
        cy.get(events.People_Tab()).should('exist').and('be.visible')
        cy.get(events.Exhibitors_Tab()).should('exist').and('be.visible')
        cy.get(events.Search_Container())
            .should('exist').and('be.visible').next().find('p').should('have.text', utils.event_search_matched)
        cy.get(events.Day_Container()).should('be.visible')


        /** this function move and compare array data with the values on the day container, making this more dynamic
         * exist multiple ways to do this, json file or calling and enpoint call to check info from the backend also.*/

        days.array1.forEach(myFunction);
        function myFunction(value, index, array) {
            cy.get(events.Day_Container()).find('button').eq(index).should('contain', array[index])
        }


    })

    it('Search title and location exist', () => {
        /*
        * typing text into search field and verify if the content exist using contains assertions
        */
        cy.get(events.Search_Container())
            .should('exist').and('be.visible').next().find('p').should('have.text', utils.event_search_matched)
        cy.get(events.Search_Placeholder()).type(utils.Event_searched_text).type('{enter}')
        cy.wait(3000)
        cy.get(events.Search_Container()).next().find('p').should('have.text', utils.event_searched_1)
        cy.get(events.First_content()).find('p').should('contain', utils.Event_searched_text)

    })

    it('Search title and location doesnt exist', () => {

        /*
        *   searching value that doesn't match and getting a value 0 session available 
        */
        cy.get(events.Search_Placeholder()).clear()
        cy.get(events.Search_Placeholder()).type(utils.event_searched_text_SP).type('{enter}')
        cy.wait(500)
        cy.get(events.Search_Container()).next().find('p').should('have.text', utils.event_searched_0)

    })



    it('Verify Morresier logos and footers', () => {
        /*
        *Just verification for the company components are visibles on the web portal
        */
        cy.get(morresierInfo.Logo()).should('be.visible')
        cy.get(morresierInfo.Icon()).should('be.visible')
        cy.get(morresierInfo.Title()).should('be.visible')
        cy.get('p').should('contain', 'Follow us').and('be.visible')
        cy.get(morresierInfo.Twitter()).should('be.visible')
        cy.get(morresierInfo.Medium()).should('be.visible')
        cy.get(morresierInfo.Company()).should('be.visible')
        cy.get(morresierInfo.Facebook()).should('be.visible')
    })


    it('Verify Days filters', () => {

        days.array1.forEach(myFunction);
        /*
        *calling this function for each day hardcoded to perform actions and validations according with the text
        */
        function myFunction(value, index, array) {
            cy.get(events.Day_Container()).find('button').eq(index).then(($div) => {
                const text = $div.text()
                switch (text) {
                    case 'Show All':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()

                        break;
                    case 'Thu, Sep 24':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[0])
                        break;
                    case 'Fri, Sep 25':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[1])
                        break;
                    case 'Sat, Sep 26':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[2])
                        break;
                    case 'Tue, Sep 29':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[3])
                        break;
                    case 'Wed, Sep 30':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[4])
                        break;
                    case 'Tue, Dec 8':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[5])
                        break;
                    case 'Tue, Jan 12':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[6])
                        break;
                    case 'Fri, Feb 12':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[7])
                        break;
                    case 'Thu, Feb 18':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[8])
                        break;
                    case 'Sun, Feb 28':
                        events.classfocusedDay(index, array[index])
                        events.CompareItems()
                        session.CheckValueSessionPage(days.array2[9])
                        break;
                    default:
                    // code block
                }
            })
        }
    })

})





