/*
* Define Selectors 
*/
const SELECTORS = {
    WEB: {
        SEARCH_FIELD: 'input[placeholder="Search"]',
        FILTER_BUTTON: '.style__FilterToggleButton-sc-1x1tqvy-6',
        STANDARD_CONTAINER: '.Container__StandardContainer-fs5mag-1',
        SEARCH_FILTER_CONTAINER: '.style__SearchAndFiltersContainer-sc-1fffutc-10',
        POSTER_GRID_CONTAINER: '.PosterGrid__StyledPosterGridContainer-jugd3x-0',
        POSTER_SUBMISSION_ARTICLE: '[aria-label="Poster: Hydration-Enhanced Lubricating Electrospun Nanofibrous Membranes Prevent Tissue Adhesion"]',
        POSTER_CARD_SUBMISSION: '.PosterCard__StyledRowContainer-p36bsq-0',
        EVENT_PAGE_POSTER_TITLE: '[data-test-id="EVENT_PAGE_FIRST_POSTER_TITLE"]',
        CONTAINER_ARTICLES: '.kuJevE',
        PAGINATION: '.style__PaginationControlsWrapper-n0rl9d-0',
        FILTER_KEYWORDS:'input[name="filter-Keywords"]',
        CHECK_LIST__PARENT_COMPONENT:'.MuiCollapse-entered',
        LIST_BUTTON:'.MuiListItem-button',
        FILTER_AUTHOR_NAME:'input[name="filter-Author names"]'

    }
}

class submissionPage {
    /*
    * Define Properties 
    */
    SearchField() {
        return (SELECTORS.WEB.SEARCH_FIELD)
    }
    FilterButton() {
        return (SELECTORS.WEB.FILTER_BUTTON)
    }

    StandarContainer() {
        return (SELECTORS.WEB.STANDARD_CONTAINER)
    }
    SearchFilterContainer() {
        return (SELECTORS.WEB.SEARCH_FILTER_CONTAINER)
    }
    PosterGridContainer() {
        return (SELECTORS.WEB.POSTER_GRID_CONTAINER)
    }
    Pagination() {
        return (SELECTORS.WEB.PAGINATION)
    }
    PosterSubmissionArticle() {
        return (SELECTORS.WEB.POSTER_SUBMISSION_ARTICLE)
    }
    PosterCardSubmission() {
        return (SELECTORS.WEB.POSTER_CARD_SUBMISSION)
    }

    EventPagePosterTitle() {
        return (SELECTORS.WEB.EVENT_PAGE_POSTER_TITLE)
    }
    ContainerArticle() {
        return (SELECTORS.WEB.CONTAINER_ARTICLES)
    }
    FilterKeywords(){
        return (SELECTORS.WEB.FILTER_KEYWORDS)
    }
    ListButton(){
        return (SELECTORS.WEB.LIST_BUTTON)
    }
    ParentComponent(){
        return (SELECTORS.WEB.CHECK_LIST__PARENT_COMPONENT)
    }

    AuthorName(){
        return (SELECTORS.WEB.FILTER_AUTHOR_NAME)
    }
    
    //this function compare the items in the label for submission with the total of childelements inside of the grid component
    CompareItems(){
        cy.get('.kuJevE',{timeout:60000}).then(($Items) => {
            var countGrid = $Items[0].childElementCount//taking the grid component and positioning on element[0] and counting all the childs elements 
            cy.get('.style__SearchAndFiltersContainer-sc-1fffutc-10').next().invoke('text').then((FilterItems) =>{
                expect(FilterItems).to.contains(countGrid)//then move the search component and take the  text to compare the grid value
            });
          })
    }
}




/** */

module.exports = new submissionPage()
