const events = require('./events.js')
/*
* Define Selectors 
*/
const SELECTORS = {
    WEB: {
        SESSION_DATE: '.style__HeroInfoLine-lrpciz-4',
        TOP_BAR: '.style__TopBarLeft-lrpciz-19'
    }
}

class sessionPage {
    /*
    * Define Properties 
    */
    SessionDate() {
        return (SELECTORS.WEB.SESSION_DATE)
    }
    TopBar() {
        return (SELECTORS.WEB.TOP_BAR)
    }

    CheckValueSessionPage(date) {
        cy.get(events.First_content()).find('> div').first().click()
        cy.wait(1000)
        cy.get(cy.get(this.SessionDate()).find('time').then(($div) => {
            expect($div).to.have.text(date)
        }))
        cy.get(this.TopBar()).find('a').click()
    }
}
module.exports = new sessionPage()
