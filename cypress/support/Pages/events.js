
const SELECTORS={
    /*
    * Define selectors
    */
    WEB:{
        CONFERENCE_LOGO:'[alt="user avatar"]',
        SIGN_UP_MODAL:'.ReactModalPortal',
        CLOSE_MODAL_BUTTON:'svg.sc-fHCHSn.fBHsIH',
        CONFERENCE_TITLE:'.style__EventTitle-sc-1fffutc-5',
        SECOND_HEADER:'.style__SecondHeaderRow-sc-1fffutc-6',
        JOIN_DISCUSSION:'.style__ChannelListWrapper-sc-1fffutc-17',
        SESSION_TAB:'#sessions',
        SUBMISSION_TAB:'#submissions',
        PEOPLE_TAB:'#people',
        EXHIBITORS_TAB:'#exhibitors',
        SEARCH_CONTAINER:'.style__StyledSearchBar-sc-1fffutc-11',
        SEARCH_LABEL:'input[placeholder="Search titles and locations"]',
        DAY_CONTAINER:'.SessionContainer__DayFiltersWrapper-sc-1bqy2jc-1',
        FIRST_CONTENT:'.bYOGXf'
    }
}
class eventPage{
    /*
    *Define properties 
    */
    conferenceLogo(){
        return(SELECTORS.WEB.CONFERENCE_LOGO)
    }
    signUpModal(){
        return(SELECTORS.WEB.SIGN_UP_MODAL)
    }
    CloseModalButton(){
        return(SELECTORS.WEB.CLOSE_MODAL_BUTTON)
    }
    conferenceTitle(){
        return(SELECTORS.WEB.CONFERENCE_TITLE)
    }
    SecondHeader(){
        return(SELECTORS.WEB.SECOND_HEADER)
    }
    JoinDiscussion(){
        return(SELECTORS.WEB.JOIN_DISCUSSION)
    }

    Session_Tab(){
        return(SELECTORS.WEB.SESSION_TAB)
    }
    Submission_Tab(){
        return(SELECTORS.WEB.SUBMISSION_TAB)
    }
    People_Tab(){
        return(SELECTORS.WEB.PEOPLE_TAB)
    }
    Exhibitors_Tab(){
        return(SELECTORS.WEB.EXHIBITORS_TAB)
    }
    Search_Container(){
        return(SELECTORS.WEB.SEARCH_CONTAINER)
    }
    Search_Placeholder(){
        return(SELECTORS.WEB.SEARCH_LABEL)
    }
    Day_Container(){
        return(SELECTORS.WEB.DAY_CONTAINER)
    }
    First_content(){
        return(SELECTORS.WEB.FIRST_CONTENT)
    }

    classfocusedDay(index,array){
        /*
         * extract text and compare with array value saved on js file, this could be replace with more time for a route call
         * validation of the focused item has the class related with focus
         */
        cy.get(this.Day_Container())
        .find('button').eq(index)
        .should('contain',array[index]).click()
        .and('have.class','jFAogO')
    }

    CompareItems(){
        /*
        * taking the grid component and positioning on element[0] and counting all the childs elements
        * then move the search component and take the  text to compare the grid value
        */
        cy.get('.bYOGXf',{timeout:60000}).then(($Items) => {
            var countGrid = $Items[0].childElementCount// 
            cy.get('.style__StyledSearchBar-sc-1fffutc-11').next().invoke('text').then((FilterItems) =>{
                expect(FilterItems).to.contains(countGrid)
            });
          })
    }
}

module.exports = new eventPage()