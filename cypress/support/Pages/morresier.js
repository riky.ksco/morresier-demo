const SELECTORS={
    /*
    * Define Selectors 
    */
    WEB:{
        LOGO:'[alt="Morressier Logo"]',
        ICON:'[alt="Morressier Icon"]',
        TITLE:'[alt="Morressier"]',
        TWITTER:'[href="https://twitter.com/morressier"]',
        MEDIUM:'[href="https://medium.com/@Morressier"]',
        COMPANY:'[href="https://www.linkedin.com/company/morressier"]',
        FACEBOOK:'[href="https://www.facebook.com/Morressier"]'

    }
}

class MorresierPage{
    /*
    * Define Properties 
    */
    Logo(){
        return(SELECTORS.WEB.LOGO)
    }
    Icon(){
        return(SELECTORS.WEB.ICON)
    }
    Title(){
        return(SELECTORS.WEB.TITLE)
    }
    Twitter(){
        return(SELECTORS.WEB.TWITTER)
    }
    Medium(){
        return(SELECTORS.WEB.MEDIUM)
    }
    Company(){
        return(SELECTORS.WEB.COMPANY)
    }

    Facebook(){
        return(SELECTORS.WEB.FACEBOOK)
    }
    }


/** */

module.exports = new MorresierPage()
