const SELECTORS={
    /**
     * Define selectors
     */
    WEB:{
        SECTION_CONTAINER_ARTICLE:'.styles__SectionContainer-kg3h9e-10',
        ARTICLE_SUMMARY_CONTAINER:'.styles__ArticleSummary-kg3h9e-4',
        PLAYER_COMPONENT:'[data-vjs-player="true"]',
        PRESENTATION_AT:'.styles__VideoInfoColumn-kg3h9e-15',
        PDF_COMPONENT:'[type="application/pdf"]',
        FULL_DESCRIPTION:'section:nth-child(5) > div > div > div > div > p > span',
        KEYWORDS_COMPONENT:'div > section:nth-child(7)',
        LICENSE_COMPONENT:'.LicenseSection__CardContent-sc-1ya4ge2-0',
        PLAYER:'[aria-label="Video Player"]',
        LOADING_SPINNER:'.vjs-loading-spinner',
        PLAY_DIV:'[title="Play Video"]'





    }
}

class articlePage{
    /*
    * Define properties 
    */
    SectionContainerArtice(){
        return(SELECTORS.WEB.SECTION_CONTAINER_ARTICLE)
    }
    ArticleSummaryContainer(){
        return(SELECTORS.WEB.ARTICLE_SUMMARY_CONTAINER)
    }

    PlayerComponent(){
        return(SELECTORS.WEB.PLAYER_COMPONENT)
    }

    PresentationAt(){
        return(SELECTORS.WEB.PRESENTATION_AT)
    }
    
    PDFComponent(){
        return(SELECTORS.WEB.PDF_COMPONENT)
    }
    
    FullDescription(){
        return(SELECTORS.WEB.FULL_DESCRIPTION)
    }
    KeywordsComponent()
    {
        return(SELECTORS.WEB.KEYWORDS_COMPONENT)
    }
    LicenseComponent()
    {
        return(SELECTORS.WEB.LICENSE_COMPONENT)
    }
    
    Player()
    {
        return(SELECTORS.WEB.PLAYER)
    }

    LoadingSpinner()
    {
        return(SELECTORS.WEB.LOADING_SPINNER)
    }  

    PlayDiv()
    {
        return(SELECTORS.WEB.PLAY_DIV)
    }
    
}


module.exports = new articlePage()
