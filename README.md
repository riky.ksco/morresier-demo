# Morresier Cypress Demo 

This is demo  app used to showcase [Cypress.io](https://www.cypress.io/) testing. this app run the following test cases on this page https://next.morressier.com/event/ictf2021/5f33b6be11826e46af50a9a7:
###  Verify landing page screen
    1)Verify Event Landing Page sections
    2)Search title and location exist
    3)Search title and location doesnt exist
    4)Verify Morresier logos and footers
    5)Verify Days filters

###  Verify landing page screen
    1)Verify Submission overview
    2)Verify Article content
    3)Search Article
    4)search not exist value
    5)Verify filter component
    6)Verify Playback

### 1. Run the tests 

```bash
## clone this repo to a local directory
git clone https://gitlab.com/riky.ksco/morresier-demo

## cd into the cloned repo
cd morresier-demo

## install the node_modules
npm install

## cypress open  with this flag false to make the test works--env failOnSnapshotDiff=false
npm run test

## cypress open  with this flag false to make the test works--env failOnSnapshotDiff=false --browser chrome
npm run runHeadless
```

### 2. Install & write tests in Cypress

[Follow these instructions to install and write tests in Cypress.](https://on.cypress.io/installing-cypress)

### 3. How the test looks running with the server locally
![event Test](https://gyazo.com/a7ae338a01ddd53558c7c427dcf3b834)
![Submission Test](https://gyazo.com/719b501b9435703ab179b49a4d0bbcf3)
![Player Test](https://gyazo.com/5d5dc926a4d55d101a6fdac0b19db6f8)



### 4. How the test looks running with the server locally
I set up a **.gitlab-ci.yml** file that include the configuration to run headless after any changes to check if the test works correctly integrated with **image: cypress/browsers:node11.13.0-chrome73**
![Gitlab CI Test](https://gyazo.com/477f620c5284f113f5d9161579448632)


